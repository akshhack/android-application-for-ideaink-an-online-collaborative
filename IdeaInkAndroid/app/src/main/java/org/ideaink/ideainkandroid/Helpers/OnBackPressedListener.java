package org.ideaink.ideainkandroid.Helpers;

public interface OnBackPressedListener {
    void onBackPressed();
}
