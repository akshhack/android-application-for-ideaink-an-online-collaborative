package org.ideaink.ideainkandroid.Toolbox;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.ideaink.ideainkandroid.R;

public interface PageTransformationAnimations {

    final String TAG = "PAGETRANS";

    class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

    abstract class BaseTransformer implements ViewPager.PageTransformer {

        abstract void onTransform(View view, float position);

        @Override
        public void transformPage(View view, float position) {
            onPreTransform(view, position);
            onTransform(view, position);
            onPostTransform(view, position);
        }

        boolean hideOffscreenPages() {
            return true;
        }

        boolean isPagingEnabled() {
            return false;
        }

        void onPreTransform(View view, float position) {
            final float width = view.getWidth();

            view.setRotationX(0);
            view.setRotationY(0);
            view.setRotation(0);
            view.setScaleX(1);
            view.setScaleY(1);
            view.setPivotX(0);
            view.setPivotY(0);
            view.setTranslationY(0);
            view.setTranslationX(isPagingEnabled() ? 0f : -width * position);

            if (hideOffscreenPages()) {
                view.setAlpha(position <= -1f || position >= 1f ? 0f : 1f);
            } else {
                view.setAlpha(1f);
            }
        }

        void onPostTransform(View view, float position) {
        }
    }

    class AccordionTransformer extends BaseTransformer {

        @Override
        protected void onTransform(View view, float position) {
            view.setPivotX(position < 0 ? 0 : view.getWidth());
            view.setScaleX(position < 0 ? 1f + position : 1f - position);
        }
    }

    class StackTransformer extends BaseTransformer {

//        private Context context;
//
//        public StackTransformer(Context context) {
//            this.context = context;
//        }

        @Override
        protected void onTransform(View view, float position) {
//            ImageView image = (ImageView) view.findViewById(R.id.featureImage);
//            if (image != null && context != null) {
//                if (position > 0) {
//                    image.setAlpha(0f);
//                } else if (position < 0) {
//                    image.setAlpha(0f);
//                } else {
//                    Animation fadein = AnimationUtils.loadAnimation(context, R.anim.image_fade_in);
//                    image.startAnimation(fadein);
//                }
//            }
            view.setTranslationX(position < 0 ? 0f : -view.getWidth() * position);
        }
    }

    class ParallaxPageTransformer extends BaseTransformer {

        private final int viewToParallax;

        public ParallaxPageTransformer(final int viewToParallax) {
            this.viewToParallax = viewToParallax;
        }

        @Override
        protected void onTransform(View view, float position) {
            int pageWidth = view.getWidth();


            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(1);

            } else if (position <= 1) { // [-1,1]

                view.findViewById(viewToParallax).setTranslationX(-position * (pageWidth / 2)); //Half the normal speed

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(1);
            }
        }
    }
}
