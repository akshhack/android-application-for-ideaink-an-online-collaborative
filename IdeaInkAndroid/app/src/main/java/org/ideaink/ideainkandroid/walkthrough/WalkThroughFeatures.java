package org.ideaink.ideainkandroid.walkthrough;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.ideaink.ideainkandroid.R;
import org.ideaink.ideainkandroid.Toolbox.FontManager;

public class WalkThroughFeatures extends Fragment {

    private static final String TAG = "WTC";
    private static final String FRAGMENT_POSITION = "fragmentPosition";
    private WalkThroughContract.OnFragmentInteractionListener mListener;
    private final int NUM_FEATURES = 4;
    private final int MIN_FEATURES = 0;
    private String[] icons;
    private String[] titles;
    private String[] descs;

    public WalkThroughFeatures() {
        // Required empty public constructor
    }

    public static WalkThroughFeatures newInstance(int position) {
        WalkThroughFeatures currentFeature = new WalkThroughFeatures();
        Bundle bundle = new Bundle();
        bundle.putInt(FRAGMENT_POSITION, position);
        currentFeature.setArguments(bundle);
        return currentFeature;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView: called.");
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_walk_through_features, container, false);
        Log.d(TAG, "onCreateView: " + getActivity() + ", " + getActivity().getResources());
        if (getActivity() != null &&
                getActivity().getResources() != null && getArguments() != null) {
            // -1 added to handle array accesses
            int position = getArguments().getInt(FRAGMENT_POSITION);
            Log.d(TAG, "onCreateView: position = " + position);
            initialiseArrays();
            setFeatureFields(position, mView);
            return mView;
        }
        return null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof WalkThroughContract.OnFragmentInteractionListener) {
            mListener = (WalkThroughContract.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: called.");
        mListener = null;
    }

    private void initialiseArrays() {
        this.icons = new String[NUM_FEATURES];
        icons[0] = getActivity().getResources().getString(R.string.fa_pencil);
        icons[1] = getActivity().getResources().getString(R.string.fa_phone);
        icons[2] = getActivity().getResources().getString(R.string.fa_sync);
        icons[3] = getActivity().getResources().getString(R.string.fa_lock);

        this.titles = new String[NUM_FEATURES];
        titles[0] = getActivity().getResources().getString(R.string.drawing_pad);
        titles[1] = getActivity().getResources().getString(R.string.video_calls);
        titles[2] = getActivity().getResources().getString(R.string.real_time);
        titles[3] = getActivity().getResources().getString(R.string.secure);

        this.descs = new String[NUM_FEATURES];
        descs[0] = getActivity().getResources().getString(R.string.drawing_pad_desc);
        descs[1] = getActivity().getResources().getString(R.string.video_calls_desc);
        descs[2] = getActivity().getResources().getString(R.string.real_time_desc);
        descs[3] = getActivity().getResources().getString(R.string.secure_desc);
    }

    private void setFeatureFields(int position, View mView) {
        // Get all content views that need to be altered dynamically.
        TextView featureIcon = (TextView) mView.findViewById(R.id.featureIcon);
        TextView featureTitle = (TextView) mView.findViewById(R.id.featureTitle);
        TextView featureDesc = (TextView) mView.findViewById(R.id.featureDesc);

        if (position >= MIN_FEATURES && position < NUM_FEATURES && getActivity() != null) {
            featureIcon.setTypeface(FontManager.getTypeface(getActivity(),
                    FontManager.FONTAWESOME));
            featureIcon.setText(icons[position]);
            featureTitle.setText(titles[position]);
            featureDesc.setText(descs[position]);
        }
    }
}
