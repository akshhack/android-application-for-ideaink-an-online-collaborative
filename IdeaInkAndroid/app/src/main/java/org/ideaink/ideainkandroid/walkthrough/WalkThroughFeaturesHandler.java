package org.ideaink.ideainkandroid.walkthrough;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.rd.PageIndicatorView;

import org.ideaink.ideainkandroid.Helpers.OnBackPressedListener;
import org.ideaink.ideainkandroid.R;
import org.ideaink.ideainkandroid.Toolbox.BitmapManager;
import org.ideaink.ideainkandroid.Toolbox.FontManager;
import org.ideaink.ideainkandroid.Toolbox.PageTransformationAnimations;

public class WalkThroughFeaturesHandler extends Fragment implements OnBackPressedListener,
        View.OnClickListener {

    private static final String TAG = "WTFH";
    private static final int NUM_PAGES = 4;
    private ViewPager mPager;
    private Bitmap bitmap;
    private ImageView featureImage;
    private TextView leftChevron;
    private TextView rightChevron;
    private int[] imageResources;

    private WalkThroughContract.OnFragmentInteractionListener mListener;

    public WalkThroughFeaturesHandler() {
        // Required empty public constructor
    }

    public static WalkThroughFeaturesHandler newInstance() {
        return new WalkThroughFeaturesHandler();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (getActivity() != null) {
            View mView = inflater.inflate(R.layout.fragment_walk_through_features_handler, container, false);
            featureImage = (ImageView) mView.findViewById(R.id.featureImage);
            leftChevron = (TextView) mView.findViewById(R.id.pagerLeftChevron);
            leftChevron.setTypeface(FontManager.getTypeface(getActivity(), FontManager.FONTAWESOME));
            leftChevron.setOnClickListener(this);
            rightChevron = (TextView) mView.findViewById(R.id.pagerRightChevron);
            rightChevron.setTypeface(FontManager.getTypeface(getActivity(), FontManager.FONTAWESOME));
            rightChevron.setOnClickListener(this);
            mPager = (ViewPager) mView.findViewById(R.id.walk_through_pager);
            // childFragmentManager used for nested fragment view
            PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager());
            // declare image resources.
            imageResources = new int[]{
                    R.drawable.feature1,
                    R.drawable.feature2,
                    R.drawable.feature3,
                    R.drawable.feature4};

            // call fade in for first image to cope with jittery effect and left chevron
            final Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.image_fade_in);
            bitmap = BitmapManager.decodeSampledBitmapFromResource(
                    getActivity().getResources(), imageResources[0], 120, 240);
            featureImage.setImageBitmap(bitmap);
            featureImage.startAnimation(fadeIn);
            leftChevron.setTextSize(28f);
            leftChevron.setTextColor(ContextCompat.getColor(getActivity(),
                    R.color.primaryHeaderColor));

            mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    // TODO: fade appropriate image in.

                    if (0 <= position && position < NUM_PAGES) {
                        if (getActivity() != null && getActivity().getResources() != null) {
                            bitmap = BitmapManager.decodeSampledBitmapFromResource(
                                    getActivity().getResources(), imageResources[position], 120, 240);
                            featureImage.setImageBitmap(bitmap);
                            featureImage.startAnimation(fadeIn);
                        }
                    }

                    if (position == 0) {
                        // set left favicon bigger if on first page
                        leftChevron.setTextSize(28f);
                        leftChevron.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.primaryHeaderColor));
                    } else {
                        leftChevron.setTextSize(20f);
                        leftChevron.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.backgroundPrimaryBlack));
                    }

                    if (position == NUM_PAGES - 1) {
                        // set right favicon bigger if on last page
                        rightChevron.setTextSize(28f);
                        rightChevron.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.primaryHeaderColor));
                    } else {
                        rightChevron.setTextSize(20f);
                        rightChevron.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.backgroundPrimaryBlack));

                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
            mPager.setAdapter(mPagerAdapter);
            PageIndicatorView pageIndicatorView = (PageIndicatorView) mView.findViewById(R.id.pageIndicatorView);
            pageIndicatorView.setViewPager(mPager);
            mPager.setPageTransformer(true, new PageTransformationAnimations.DepthPageTransformer());

            mListener.setBackPressedListener(this);
            return mView;
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: called.");
        if (mPager.getCurrentItem() == 0) {
            // if on the last page let the activity decide what to do
            mListener.setBackPressedListener(null);
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    private void onForwardPressed() {
        if (mPager.getCurrentItem() == NUM_PAGES - 1) {
            // Set new fragment here.
            mListener.performUIAction(WalkThroughActivity.UIAction.GOTO_GET_STARTED, null);
        } else {
            mPager.setCurrentItem(mPager.getCurrentItem() + 1);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pagerLeftChevron:
                onBackPressed();
                break;
            case R.id.pagerRightChevron:
                onForwardPressed();
                break;
            default:
                // Do nothing.
                break;
        }

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        ScreenSlidePagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                case 1:
                case 2:
                case 3:
                    return WalkThroughFeatures.newInstance(position);
                default:
                    // just return the first position fragment
                    return WalkThroughFeatures.newInstance(0);
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof WalkThroughContract.OnFragmentInteractionListener) {
            mListener = (WalkThroughContract.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
