package org.ideaink.ideainkandroid.walkthrough;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import org.ideaink.ideainkandroid.Helpers.OnBackPressedListener;
import org.ideaink.ideainkandroid.R;

public class WalkThroughActivity extends FragmentActivity implements
        WalkThroughContract.ViewOperations, WalkThroughContract.OnFragmentInteractionListener {

    private static final String TAG = "WTA";
    private OnBackPressedListener onBackPressedListener;
    private FragmentTransaction waitingTransaction;
    private boolean doNotCommitTransaction = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: called.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk_through);

        // startup view
        if (savedInstanceState == null) {
            WalkThroughIntro intro = new WalkThroughIntro();
            replaceFragment(intro, WalkThroughFragmentType.INTRO.rawValue(), true);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: called.");
        super.onSaveInstanceState(outState);
        doNotCommitTransaction = true;
        waitingTransaction = null;
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onSaveInstanceState: called.");
        super.onPause();
        doNotCommitTransaction = true;
        waitingTransaction = null;
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        doNotCommitTransaction = false;
        if (waitingTransaction != null) {
            try {
                waitingTransaction.commit();
            } catch (IllegalStateException ise) {
                ise.printStackTrace();
            }
            waitingTransaction = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null) {
            onBackPressedListener.onBackPressed();
        } else {
            if (getSupportFragmentManager().popBackStackImmediate(
                    WalkThroughFragmentType.INTRO.rawValue(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE) &&
                    getSupportFragmentManager().findFragmentByTag(
                            WalkThroughFragmentType.INTRO.rawValue()) == null) {
                    replaceFragment(new WalkThroughIntro(),
                            WalkThroughFragmentType.INTRO.rawValue(), true);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void performUIAction(UIAction uiAction, Object res) {
        switch (uiAction) {
            case GOTO_GET_STARTED:
                WalkThroughConclusion conclusion = new WalkThroughConclusion();
                replaceFragment(conclusion, WalkThroughFragmentType.GET_STARTED.rawValue(), false);
                break;
            case GOTO_FEATURE_WALKTHROUGH:
                WalkThroughFeaturesHandler featuresHandler = new WalkThroughFeaturesHandler();
                replaceFragment(featuresHandler, WalkThroughFragmentType.FEATURES.rawValue(), true);
                break;
            case GOTO_AUTHENTICATION:
                // TODO: Send person to authentication activity. Clear this activity's memory
                final String redirecting = "Redirecting to Authentication Activity";
                Toast.makeText(WalkThroughActivity.this, redirecting, Toast.LENGTH_LONG).show();
                break;
            default:
                // Do nothing
                break;
        }
    }

    @Override
    public void setBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    private void replaceFragment(Fragment frag, String fragTag, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.walk_through_fragment_container, frag, fragTag);
        if (doNotCommitTransaction) {
            Log.d(TAG, "addFragment: fragment set to waiting.");
            waitingTransaction = transaction;
        } else {
            Log.d(TAG, "addFragment: fragment committed.");
            if (addToBackStack) {
                transaction.addToBackStack(fragTag);
            }
            transaction.commit();
        }
    }

    enum UIAction {
        GOTO_GET_STARTED ("getStarted"),
        GOTO_FEATURE_WALKTHROUGH ("gotoFeatureWalkthrough"),
        GOTO_AUTHENTICATION ("gotoAuthentication");

        final String uiAction;

        UIAction(final String uiAction) {
            this.uiAction = uiAction;
        }

        String rawValue() {
            return this.uiAction;
        }
    }

    enum WalkThroughFragmentType {
        FEATURES ("features"),
        INTRO ("intro"),
        GET_STARTED ("getStarted");

        final String fragmentType;

        WalkThroughFragmentType(final String fragmentType) {this.fragmentType = fragmentType;}

        String rawValue() {return this.fragmentType;}
    }
}
