package org.ideaink.ideainkandroid.splash;

interface SplashContract {

    interface ViewOperations {
        void startSplashIcon();
        void endSplashIcon();
    }

    interface PresenterOperations {
        void startSplashIcon();
        void endSplashIcon();
    }
}
