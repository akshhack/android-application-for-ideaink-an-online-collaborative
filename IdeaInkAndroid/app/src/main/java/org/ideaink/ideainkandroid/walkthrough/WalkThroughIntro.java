package org.ideaink.ideainkandroid.walkthrough;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.ideaink.ideainkandroid.Helpers.OnBackPressedListener;
import org.ideaink.ideainkandroid.R;

public class WalkThroughIntro extends Fragment implements View.OnClickListener, OnBackPressedListener {

    // TODO: Add this fragment to backStack.

    private static final String TAG = "WTI";
    private WalkThroughContract.OnFragmentInteractionListener mListener;

    public WalkThroughIntro() {
        // Required empty public constructor
    }

    public static WalkThroughIntro newInstance() {
        return new WalkThroughIntro();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: called.");
        View mView = inflater.inflate(R.layout.fragment_walk_through_intro, container, false);
        Button cont = (Button) mView.findViewById(R.id.walk_through_intro_continue);
        cont.setOnClickListener(this);
        mListener.setBackPressedListener(this);
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof WalkThroughContract.OnFragmentInteractionListener) {
            mListener = (WalkThroughContract.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.walk_through_intro_continue:
                mListener.performUIAction(WalkThroughActivity.UIAction.GOTO_FEATURE_WALKTHROUGH,
                        null);
                break;
            default:
                // Do nothing.
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().moveTaskToBack(true);
        }
    }
}
