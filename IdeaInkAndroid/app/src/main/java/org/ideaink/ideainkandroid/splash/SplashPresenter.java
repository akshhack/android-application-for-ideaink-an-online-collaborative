package org.ideaink.ideainkandroid.splash;

import android.util.Log;

class SplashPresenter implements SplashContract.PresenterOperations{

    private static final String TAG = "SPLASH_PR";
    private SplashContract.ViewOperations splashViewOperations;

    SplashPresenter(SplashContract.ViewOperations splashViewOperations) {
        Log.d(TAG, "constructor called.");
        this.splashViewOperations = splashViewOperations;
    }

    @Override
    public void startSplashIcon() {
        if (splashViewOperations != null) {
            Log.d(TAG, "starting splashIcon animation");
            splashViewOperations.startSplashIcon();
        }
    }

    @Override
    public void endSplashIcon() {
        if (splashViewOperations != null) {
            Log.d(TAG, "starting splashIcon animation");
            splashViewOperations.endSplashIcon();
        }
    }
}
