package org.ideaink.ideainkandroid.walkthrough;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.ideaink.ideainkandroid.R;

public class WalkThroughConclusion extends Fragment implements View.OnClickListener {

    private final String TAG = "WTC";
    WalkThroughContract.OnFragmentInteractionListener mListener;
    private Button getStarted;

    public WalkThroughConclusion() {
        // Required empty public constructor
    }

    public static WalkThroughConclusion newInstance() {
        return new WalkThroughConclusion();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // so that activity is forced to "moveToBackStack" i.e. cannot go back to walkthrough features
        mListener.setBackPressedListener(null);
        View mView = inflater.inflate(R.layout.fragment_walk_through_conclusion, container, false);
        getStarted = (Button) mView.findViewById(R.id.walk_through_conclusion_continue);
        getStarted.setOnClickListener(this);
        return mView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof WalkThroughContract.OnFragmentInteractionListener) {
            mListener = (WalkThroughContract.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.walk_through_conclusion_continue:
                mListener.performUIAction(WalkThroughActivity.UIAction.GOTO_AUTHENTICATION, null);
                break;
            default:
                // Do nothing.
                break;
        }
    }
}
