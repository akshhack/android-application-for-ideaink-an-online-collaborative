package org.ideaink.ideainkandroid.walkthrough;

import org.ideaink.ideainkandroid.Helpers.OnBackPressedListener;

interface WalkThroughContract {

    interface ViewOperations {

    }

    interface PresenterOperations {

    }

    interface OnFragmentInteractionListener {
        void performUIAction(WalkThroughActivity.UIAction uiAction, Object res);
        void setBackPressedListener(OnBackPressedListener onBackPressedListener);
    }
}
