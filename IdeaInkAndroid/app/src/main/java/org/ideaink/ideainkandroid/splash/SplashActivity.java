package org.ideaink.ideainkandroid.splash;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.ideaink.ideainkandroid.R;

public class SplashActivity extends AppCompatActivity implements
        SplashContract.ViewOperations {

    private ImageView splashIcon;
    private Bitmap splashBitmap;
    private SplashContract.PresenterOperations splashPresenterOperations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setupSplashIcon();

        splashPresenterOperations = new SplashPresenter(this);

        splashPresenterOperations.startSplashIcon();
    }

    @Override
    public void startSplashIcon() {
        Animation rotateAnimation = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.rotate);
        if (splashIcon != null && rotateAnimation != null) {
            splashIcon.startAnimation(rotateAnimation);
        }
    }

    @Override
    public void endSplashIcon() {
        if (splashIcon != null) {
            splashIcon.clearAnimation();
        }
    }

    private void setupSplashIcon() {
        splashIcon = (ImageView) findViewById(R.id.splashIcon);
        splashBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.ideainklogo);
        splashIcon.setImageBitmap(Bitmap.createScaledBitmap(splashBitmap, 360, 360, false));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (splashBitmap != null) {
            splashBitmap.recycle();
            splashBitmap = null;
        }
    }
}
